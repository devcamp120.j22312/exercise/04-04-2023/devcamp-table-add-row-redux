import { CLICK_BUTTON, INPUT_STRING } from "../constants/task.constants"

export const stringChangeHandler = (inputValue) => {
    return {
        type: INPUT_STRING,
        payload: inputValue
    }
}

export const taskAddClicked = () => {
    return {
        type: CLICK_BUTTON
    }
}