import { Grid, Button, TextField, Table, TableHead, TableRow, TableCell, TableContainer, TableBody, Paper, Container } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { stringChangeHandler, taskAddClicked } from '../actions/task.action';

const Task = () => {
    const dispatch = useDispatch();

    const { inputString, taskList } = useSelector((reduxData) => {
        return reduxData.taskReducer;
    })

    const inputStringHandler = (event) => {
        dispatch(stringChangeHandler(event.target.value));
    }

    const buttonClickHandler = () => {
        dispatch(taskAddClicked());
    }

    return (
        <Container>
            <Grid container mt={5} alignItems="center">
                <Grid item xs={3} md={3} lg={3} sm={3} textAlign="center">
                    <p>Nhập nội dung dòng</p>
                </Grid>
                <Grid item xs={6} md={6} lg={6} sm={6}>
                    <TextField label="Input here" fullWidth value={inputString} onChange={inputStringHandler}></TextField>
                </Grid>
                <Grid item xs={3} md={3} lg={3} sm={3} textAlign="center">
                    <Button variant="contained" onClick={buttonClickHandler}>Thêm</Button>
                </Grid>
            </Grid>

            <TableContainer style={{ marginTop: "20px" }} component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow container>
                            <TableCell>STT</TableCell>
                            <TableCell>Nội dung</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {taskList.map((element, index) => {
                            return (
                                <TableRow container>
                                    <TableCell>{index + 1}</TableCell>
                                    <TableCell>{element.name}</TableCell>
                                </TableRow>
                            )

                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    )
}

export default Task;