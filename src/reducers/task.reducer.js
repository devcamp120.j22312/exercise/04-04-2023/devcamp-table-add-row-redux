import { CLICK_BUTTON, INPUT_STRING } from "../constants/task.constants";

const initialState = {
    inputString: "",
    taskList: []
}

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT_STRING:
            state.inputString = action.payload;
            break;
        case CLICK_BUTTON:
            state.taskList.push({
                id: state.taskList.length,
                name: state.inputString
            })
            state.inputString = "";
            break;

        default:
            break;
    }
    return {...state}
}

export default taskReducer;
